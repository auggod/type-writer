Polymer do
  is: \type-writer

  properties:
    text:
      type: Array
    clear:
      type: Boolean
      value: false
    cursor:
      type: String
      value: '\u005F'
    speed:
      type: Number
      value: 100
    delay:
      type: Number
      value: 1000
    isTyping:
      type: Boolean
      value: false
      notify: true
      observer: \_typingStatusChanged

  ready: ->

    @init!

  init: ->
    
    this.$.text.textContent = ''

    @write!

  _typingStatusChanged: (value) ->
    @toggleClass \typing, value
    this.$.cursor.classList.toggle \blink

  write: (textIndex=0) ->

    self = this

    textEl = self.$.text
    text = self.text

    self.isTyping = true

    if self.clear == true
      textEl.textContent = ''

    index = 0

    interval = setInterval ->
      index := index + 1

      if text[textIndex]
        textEl.textContent = textEl.textContent += text[textIndex].charAt(index - 1)

      if text[textIndex] and index == text[textIndex].length
        clearInterval(interval)

        self.isTyping = false

        setTimeout ->
          if textIndex < text.length
            # Continue
            textIndex := textIndex + 1
            self.write(textIndex)
          if textIndex == text.length
            # Restart
            textEl.textContent = ''
            self.write!
        , self.delay
    , self.speed
